# IAM User Terraform module

Módulo do terraform que cria usuários no IAM da AWS.

Os seguintes recursos podem ser criados pelo módulo:

* Usuário IAM

## Usage

    module "module_name" {

      source   = "git::ssh://git@gitlab.com:geovanaSouza/terraform-modules//iam-user"

      username = "<nome_usuario>"

    }

## Inputs

| Name | Description | Type | Default | Required |
|------|-------------|------|---------|:--------:|
| username | Nome do usuário à ser criado no IAM | `string` | `null` | yes |
